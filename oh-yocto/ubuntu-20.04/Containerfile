FROM quay.io/toolbx-images/ubuntu-toolbox:20.04

ARG DEBIAN_FRONTEND=noninteractive

# /bin/sh should be bash
RUN echo "dash dash/sh boolean false" | debconf-set-selections \
 && dpkg-reconfigure dash

# Install prerequisites according to
# https://docs.oniroproject.org/en/latest/oniro/oniro-quick-build.html
RUN apt-get update -q \
 && apt-get install -q -y \
      gawk wget git diffstat unzip texinfo gcc-multilib \
      build-essential chrpath socat cpio python3 python3-pip python3-pexpect \
      xz-utils debianutils iputils-ping python3-git python3-jinja2 \
      libegl1-mesa libsdl1.2-dev \
      pylint3 xterm file zstd \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

# Install repo from OSTC PPA
RUN apt-get update -q \
 && apt-get install -q -y software-properties-common \
 && add-apt-repository ppa:ostc/ppa \
 && apt-get update -q \
 && apt-get install -q -y git-repo \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

# Install additional packages not included in oniro-quick-build quide
RUN apt-get update -q \
 && apt-get install -q -y lz4 locales git-lfs \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/

# Use UTF-8 locale
RUN echo "locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8" | debconf-set-selections \
 && echo "locales locales/default_environment_locale select en_US.UTF-8" | debconf-set-selections \
 && sed -i 's/^# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen \
 && dpkg-reconfigure locales
ENV LANG=en_US.UTF-8

LABEL name="oniro-yocto-toolbox" \
      version="20.04-1" \
      summary="For doing Oniro Project Yocto builds" \
      maintainer="Esben Haabendal <esben@geanix.com>"
